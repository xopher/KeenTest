Code example for Keen

Site: https://www.keenfootwear.com/

Username: xopher@tigerburningbright.net

Password: thisis4test

Download the project, and then do the following:

Install node.js: https://nodejs.org/en/

Install grunt : npm install -g grunt

Install grunt-cli : npm install -g grunt-cli

In the root project directory, run: npm update

Download the chromedriver, and add it to your path:
https://chromedriver.storage.googleapis.com/index.html
(or use npm install chromedriver)

Start chromedriver in one console, and in another, run
tests by going to the root project directory and 
running:

grunt chrome

Running 'grunt firefox' should also work, but I haven't tried it.

