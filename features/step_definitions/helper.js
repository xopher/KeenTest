'use strict';

var webdriver = require('selenium-webdriver');
var driver = require('../support/world.js').getDriver();
var assert = require('chai').assert;


var Helper = function () {
};

Helper.prototype.findByPartialId = function (partialId) {

    var element = driver.findElement(webdriver.By.css("[id^=" + partialId + "]"));

    return element;

};

Helper.prototype.findCaseInsensitiveText = function (searchText) {
        var result = false;

        driver.findElement(webdriver.By.tagName("body")).getText().then(function (text) {
            var pageText = text.replace(/(\r\n|\n|\r)/gm, " ").toLowerCase();

            if (pageText.indexOf(searchText.toLowerCase()) > -1) {
                result = true;
            }

            assert.equal(result, true, searchText + " not found!");
            
        });

        return result;
};

module.exports = new Helper();