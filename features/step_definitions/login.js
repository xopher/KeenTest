'use strict';

var assert = require('chai').assert;
var webdriver = require('selenium-webdriver');
var helper = require('./helper.js');
var driver = require('../support/world.js').getDriver();


module.exports = function () {
  this.World = require('../support/world.js').World;


  // Step definition to login to Keen
  this.When(/^I login to Keen as "([^"]*)" with password "([^"]*)"$/, function (username, password) {

    // navigate to Keen Footwear
    driver.get('https://www.keenfootwear.com/');

    // find and click the sign in (and find a prettier xpath)
    var element = driver.findElement(webdriver.By.xpath("//*[@id='utility-nav']/div/ul[2]/li[1]/a[1]"));
    element.click();

    // find username field, enter username
    element = helper.findByPartialId("dwfrm_login_username");
    element.sendKeys(username);

    // find password field, enter password
    element = helper.findByPartialId("dwfrm_login_password");
    element.sendKeys(password);

    // find and click the login button
    driver.findElement(webdriver.By.name("dwfrm_login_login")).click();

  });

  // look for text on the page
  this.Then(/^I should see "([^"]*)"$/, function (searchText) {
    helper.findCaseInsensitiveText(searchText);
  });

  this.When(/^I go to URL "([^"]*)"$/, function (url) {
    // navigate to Keen Footwear
    driver.get(url);
  });

  this.Then(/^I select item by xpath "([^"]*)"$/, function (path) {
    // find and click the item
    var element = driver.findElement(webdriver.By.xpath(path));
    element.click();
  });

  this.Then(/^I wait for item by name "([^"]*)"$/, function (name) {
    // find item and enter value
    driver.wait(webdriver.until.elementLocated(webdriver.By.name(name)),10000);
  });

  this.Then(/^I find item by partial ID "([^"]*)" and enter "([^"]*)"$/, function (partialID, data) {
    // find item and enter value
    var element = helper.findByPartialId(partialID);
    element.sendKeys(data);
  });

  this.Then(/^I select item by name "([^"]*)"$/, function (name) {
    // find and click the item
    var element = driver.findElement(webdriver.By.name(name));
    element.click();
  });
};