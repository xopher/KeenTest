Feature: Login
    Log a user into Keen, and 
    then check that their name 
    is displaying correctly

  Scenario: Login and check name
    When I login to Keen as "xopher@tigerburningbright.net" with password "thisis4test"
    Then I should see "Hi, Christopher"

  Scenario: Login and check name - the hard way
    When I go to URL "https://www.keenfootwear.com/"
    Then I select item by xpath "//*[@id='utility-nav']/div/ul[2]/li[1]/a[1]"
    Then I wait for item by name "dwfrm_login_login"   
    Then I find item by partial ID "dwfrm_login_username" and enter "xopher@tigerburningbright.net"
    Then I find item by partial ID "dwfrm_login_password" and enter "thisis4test"
    Then I select item by name "dwfrm_login_login"
    Then I should see "Hi, Christopher"
    
  Scenario: Login and check incorrect name
    When I login to Keen as "xopher@tigerburningbright.net" with password "thisis4test"
    Then I should see "Hi, Charlotte"


